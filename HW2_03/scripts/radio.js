function check() {
    var x1 = document.getElementById("b1").checked;
    var x2 = document.getElementById("b2").checked;
    var x3 = document.getElementById("b3").checked;
    var x4 = document.getElementById("b4").checked;
    var x5 = document.getElementById("b5").checked;
    var x6 = document.getElementById("b6").checked;

    if (x1 == true) {
        document.getElementById("f1").style.zIndex = 2;
    }

    if (x2 == true) {
        document.getElementById("f2").style.zIndex = 2;
    }

    if (x3 == true) {
        document.getElementById("f3").style.zIndex = 2;
    }

    if (x4 == true) {
        document.getElementById("f4").style.zIndex = 2;
    }

    if (x5 == true) {
        document.getElementById("f5").style.zIndex = 2;
    }

    if (x6 == true) {
        document.getElementById("f1").style.zIndex = 2;
        document.getElementById("f2").style.zIndex = 2;
        document.getElementById("f3").style.zIndex = 2;
        document.getElementById("f4").style.zIndex = 2;
        document.getElementById("f5").style.zIndex = 2;
    }

    var str = document.getElementById("input").value;

    if (str != "") {
        if (str == "ragdoll") {
            document.getElementById("f1").style.zIndex = 2;
            document.getElementById("f2").style.zIndex = 2;
            document.getElementById("f5").style.zIndex = 2;
        } else if (str == "ring") {
            document.getElementById("f3").style.zIndex = 2;
        } else if (str == "tardar") {
            document.getElementById("f4").style.zIndex = 2;
        } else {
            alert("No  '" + str + "'  breed");
        }
    }

    document.getElementById("input").value = "";
}

function setup() {
    document.getElementById("b1").checked = false;
    document.getElementById("f1").style.zIndex = -1;
    document.getElementById("b2").checked = false;
    document.getElementById("f2").style.zIndex = -1;
    document.getElementById("b3").checked = false;
    document.getElementById("f3").style.zIndex = -1;
    document.getElementById("b4").checked = false;
    document.getElementById("f4").style.zIndex = -1;
    document.getElementById("b5").checked = false;
    document.getElementById("f5").style.zIndex = -1;
    document.getElementById("b6").checked = false;
    document.getElementById("f6").style.zIndex = -1;
}
